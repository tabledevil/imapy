#!/usr/bin/env python3
import imaplib, email
from email.header import decode_header
import re
import os
from configparser import ConfigParser
import dialog
# import functools
import time

class ImapConnector:
    def __init__(self,host:str,port:str,username:str,password:str):
        self.imapserver=imaplib.IMAP4_SSL(host,port)
        self.imapserver.login(username,password)

    def select(self,folder:str):
        return self.imapserver.select(folder)

    def search(self, *args):
        return self.imapserver.search(*args)

    def search_mails(self,key,value):
        # Search Mails by header field values
        _, nums = self.search(None,key,'"{}"'.format(value))
        return nums[0].split()

    def delete_mail(self,num):
        num=bytes(str(int(num)),'ascii')
        self.imapserver.store(num, '+FLAGS', '\\Deleted')
        self.imapserver.expunge()

    # @functools.lru_cache()
    def get_mail(self,num):
        num=bytes(str(int(num)),'ascii')
        res, data = self.imapserver.fetch(num,'(RFC822)')
        try:
            eml=email.message_from_bytes(data[0][1])
            return eml
        except:
            return None

    def show_mail(self,num):
        eml=self.get_mail(num)
        dia.scrollbox(str(eml))

    # @functools.lru_cache()
    def get_mailboxes(self):
        status, data = self.imapserver.list()
        if status=='OK':
            for folder in data:
                original=self.force_decode(folder)
                folder=re.compile('^\(([^)]+)\)\s+"([^\"]+)\"\s+\"([^\"]+)\"')
                res=folder.findall(original)
                flags=[x.lstrip("\\") for x in res[0][0].split(" ")]
                delimiter=res[0][1]
                label=res[0][2]
                label_decoded=bytes(label.replace('&A','+A').replace("&-A","+A"),'ascii').decode('utf7')
                yield label,label_decoded,flags,delimiter

    def get_mailbox_list(self):
        for label,label_decoded,flags,delimiter in self.get_mailboxes():
            yield label_decoded

    def select_mailbox(self,mailbox_name):
        for label,label_decoded,_ in self.get_mailboxes():
            if mailbox_name==label_decoded:
                return self.select(label)

    def select_mailbox(self,switch=True):
        mailboxes=[x for x in self.get_mailboxes()]
        list_of_mailboxes=[(str(id),label_decoded) for id,(label,label_decoded,flags,delimiter) in enumerate(mailboxes)]
        code,selection=dia.menu("Select Folder",choices=list_of_mailboxes)
        if code=="ok":
            # selectedfolder=bytes(selectedfolder,'utf7').decode('ascii').replace('+A','&A')
            encoded_foldername=mailboxes[int(selection)][0]
            decoded_foldername=mailboxes[int(selection)][1]
            if switch==True:
                return code, decoded_foldername, self.select(encoded_foldername)
            else:
                return code, decoded_foldername, None
        else:
            return code, None, 0

    def get_mail_list(self):
        typ, nums = self.search(None, 'ALL')
        for n in nums[0].split():
            subject_line=self.get_subject(n)
            yield (int(n),subject_line)

    def get_header(self, eml:email, header:str):
        '''Retrieves header for an eml '''
        a=email.header.decode_header(eml[header])
        ergebnisse=[]
        for eintrag in a:
            ergebnisse.append(self.force_decode(eintrag[0]))
        return ergebnisse

    def force_decode(self, string, codecs=['utf8', 'cp1252' ,'utf16']):
        if isinstance(string, str):
            return string
        for i in codecs:
            try:
                return string.decode(i)
            except UnicodeDecodeError:
                pass
        raise Exception("Could not decode")

    def decode(self, data):
        if isinstance(data,bytes):
            data=self.force_decode(data)
        tmp=email.header.decode_header(data)
        res=""
        for part in tmp:
            if part[1]==None:
                if isinstance(part[0],str):
                    res+= part[0]
                else:
                    try:
                        res+= part[0].decode('ascii')
                    except:
                        print(part[0])
            else:
                res+= part[0].decode(part[1])
        return " ".join(res.split())

    def retrieve(self,num,field):
        num=bytes(str(int(num)),'ascii')
        res, data = self.imapserver.fetch(num,"BODY.PEEK[HEADER.FIELDS ({})]".format(field))
        if data and data[0]:
            x,y = data[0]
            y=self.force_decode(y)
            y=y.split(":",1)
            y=y[1]
            return self.decode(y)

    def get_subject(self,num):
        y=self.retrieve(num,"SUBJECT")
        z=self.retrieve(num,"FROM")
        return "{} von <{}>".format(y,z)

    def scan_for_marvins(self,eml):
        """Scrape email body for matches of regular expression-pattern"""
        marvin_candidates=re.compile('(?:[mM][aA][rR][vV][iI][nN].{0,3})?(\d{14})')
        texttosearch="\n".join(self.get_header(eml,'Subject'))
        for part in eml.walk():
            if 'text/plain' == part.get_content_type():
                texttosearch+="\n"+self.force_decode(part.get_payload(decode=True))
        results=marvin_candidates.findall(texttosearch)
        ergebnisse=[]
        for x in results:
            if x not in ergebnisse:
                ergebnisse.append(x)
        return ergebnisse

    def edit_mail(self,num):
        eml=self.get_mail(num)
        old_subject=self.get_header(eml,'Subject')[0]
        results=self.scan_for_marvins(eml)
        suggesttext="Found {} possible marvins".format(len(results))
        suggesttext+="\n"
        suggesttext+="\n".join(results)
        if len(results)>0:
            suggested_subject="MARVIN#{}_{}".format(results[0],old_subject)
        else:
            suggested_subject="MARVIN#2020xxxx75xxxx_{}".format(old_subject)
        action,new_subject=dia.inputbox(suggesttext,init=suggested_subject)
        eml.replace_header('Subject',new_subject)
        status,d = self.imapserver.append('INBOX','', imaplib.Time2Internaldate(time.time()),str(eml).encode('utf-8'))
        if "OK" in status:
            self.delete_mail(num)



def get_config(config_file_path):
    if not os.path.isfile(config_file_path):
        config_instance = ConfigParser()
        config_instance.add_section("CREDENTIALS")
        config_instance.add_section("SERVER")
        config_instance["CREDENTIALS"]["username"] = "virus-user"
        config_instance["CREDENTIALS"]["password"] = "whambamBW"
        config_instance["SERVER"]["host"] = "mail.server.dom"
        config_instance["SERVER"]["port"] = "993"
        config_instance["SERVER"]["mailbox"] = "INBOX"
        with open(config_file_path, 'w') as conf:
            config_instance.write(conf)
        print("No Config found!")
        print("Example Config written to {}".format(config_file_path))
        print("Please Edit and Repeat")
        exit(1)
    else:
        config_instance = ConfigParser()
        config_instance.read(config_file_path)
        return config_instance

def main():
    config_file_path=os.path.join(os.path.expanduser('~'),".imap_virus_marvin.ini")
    marvin_pattern=re.compile('MARVIN#\d{14}_')

    config=get_config(config_file_path)
    connection=ImapConnector(config["SERVER"]["host"],config["SERVER"]["port"],config["CREDENTIALS"]["username"],config["CREDENTIALS"]["password"])

    code, mb, count = connection.select_mailbox()
    while code!="cancel" and code!="esc":
        while code!="cancel" and code!="esc":
            code,selection=dia.menu("Edit Subject - {}".format(mb),choices=[(str(x),y) for x,y in connection.get_mail_list()],extra_button=True,extra_label="Show",ok_label="Edit",cancel_label="Back")

            if code=="ok":
                connection.edit_mail(selection)
            if code=="extra":
                connection.show_mail(selection)
        code, mb, count = connection.select_mailbox()

if __name__ == "__main__":
    try:
        global dia
        dia=dialog.Dialog(autowidgetsize=True)
        dia.set_background_title("IMAP-Mail-Renamer")
    except:
        print("Dialog binary not found")
        print("try: sudo apt install dialog")
        exit(1)
    main()
